package hello;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "gispdamUsersAndroid", path = "gispdamUsersAndroid")
public interface GispdamUsersAndroidRepository extends PagingAndSortingRepository<GispdamUsersAndroid, Integer> {

	public Optional<GispdamUsersAndroid> findByLoginName(String username);

}
