package hello;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class GispdamCollectDetail2 {
	@Id
	private String id;
	private String dateCreated;
	private String dateFile;
	private String description;
	private String displayName;
	private String fileName;
	private String idheader;
	private String kodeCabang;
	private String dateUploaded;
	private String extendName;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getDateFile() {
		return dateFile;
	}
	public void setDateFile(String dateFile) {
		this.dateFile = dateFile;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getIdheader() {
		return idheader;
	}
	public void setIdheader(String idheader) {
		this.idheader = idheader;
	}
	public String getKodeCabang() {
		return kodeCabang;
	}
	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}
	public String getDateUploaded() {
		return dateUploaded;
	}
	public void setDateUploaded(String dateUploaded) {
		this.dateUploaded = dateUploaded;
	}
	public String getExtendName() {
		return extendName;
	}
	public void setExtendName(String extendName) {
		this.extendName = extendName;
	}
	@Override
	public String toString() {
		return "GispdamCollectDetail2 [id=" + id + ", dateCreated=" + dateCreated + ", dateFile=" + dateFile
				+ ", description=" + description + ", displayName=" + displayName + ", fileName=" + fileName
				+ ", idheader=" + idheader + ", kodeCabang=" + kodeCabang + ", dateUploaded=" + dateUploaded
				+ ", extendName=" + extendName + "]";
	}	
	
	
}
