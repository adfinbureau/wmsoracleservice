package hello;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class GispdamPhotoCollect {
	@Id
	private String id;
	private String idstyle;
	private String kodeCabang;
	private String latitude;
	private String logitude;
	private String name;
	private String idsurvey;
	private String idAndroid;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdstyle() {
		return idstyle;
	}
	public void setIdstyle(String idstyle) {
		this.idstyle = idstyle;
	}
	public String getKodeCabang() {
		return kodeCabang;
	}
	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLogitude(String logitude) {
		this.logitude = logitude;
	}
	
	public String getLogitude() {
		return logitude;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIdsurvey() {
		return idsurvey;
	}
	public void setIdsurvey(String idsurvey) {
		this.idsurvey = idsurvey;
	}
	public String getIdAndroid() {
		return idAndroid;
	}
	public void setIdAndroid(String idAndroid) {
		this.idAndroid = idAndroid;
	}
	@Override
	public String toString() {
		return "GispdamPhotoCollect [id=" + id + ", idstyle=" + idstyle + ", kodeCabang=" + kodeCabang + ", latitude="
				+ latitude + ", logitude=" + logitude + ", name=" + name + ", idsurvey=" + idsurvey + ", idAndroid="
				+ idAndroid + "]";
	}
	
	
}
