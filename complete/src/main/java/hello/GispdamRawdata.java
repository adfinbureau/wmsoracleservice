package hello;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(value=GispdamRawdata.LongLatId.class)
public class GispdamRawdata {
	@Id
	private String latitude;
	@Id
	private String longitude;
	private String elevasi;
	private String codepdam;
	private String no;
	private String remark;
	
	 static class LongLatId implements Serializable {
		private String latitude;
		private String longitude;
		public String getLatitude() {
			return latitude;
		}
		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}
		public String getLongitude() {
			return longitude;
		}
		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}
    }
	 
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getElevasi() {
		return elevasi;
	}
	public void setElevasi(String elevasi) {
		this.elevasi = elevasi;
	}
	public String getCodepdam() {
		return codepdam;
	}
	public void setCodepdam(String codepdam) {
		this.codepdam = codepdam;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "GispdamRawdata [latitude=" + latitude + ", longitude=" + longitude + ", elevasi=" + elevasi
				+ ", codepdam=" + codepdam + ", no=" + no + ", remark=" + remark + "]";
	}

	
	
}
