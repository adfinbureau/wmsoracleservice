package hello;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "gispdamCollectDetail3", path = "gispdamCollectDetail3")
public interface GispdamCollectDetail3Repository extends PagingAndSortingRepository<GispdamCollectDetail3, String> {

	GispdamCollectDetail3 findOneByIdheader(String id);

	GispdamCollectDetail3 findOneByFileName(String fileName);

}
