package hello;

public class GispdamResponse {
	private String id;
	private String fileName;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "GispdamResponse [id=" + id + "]";
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getFileName() {
		return fileName;
	}	
	
}
