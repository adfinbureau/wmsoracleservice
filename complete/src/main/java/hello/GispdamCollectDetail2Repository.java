package hello;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "gispdamCollectDetail2", path = "gispdamCollectDetail2")
public interface GispdamCollectDetail2Repository extends PagingAndSortingRepository<GispdamCollectDetail2, String> {

	GispdamCollectDetail2 findOneByIdheader(String id);

	GispdamCollectDetail2 findOneByFileName(String fileName);

}
