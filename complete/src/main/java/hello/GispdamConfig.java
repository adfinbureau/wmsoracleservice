package hello;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class GispdamConfig {
	@Id
	private int id;
	private String name;
	private String value;
	private String name2;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getName2() {
		return name2;
	}
	public void setName2(String name2) {
		this.name2 = name2;
	}
	
	
}
