package hello;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class GispdamUsersAndroid {
	@Id
	private int id;
	private String kodeCabang;
	private String loginName;
	private String password;
	private String anroidImei;
	private int userStatus;
	private String idsurvey;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKodeCabang() {
		return kodeCabang;
	}
	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAnroidImei() {
		return anroidImei;
	}
	public void setAnroidImei(String anroidImei) {
		this.anroidImei = anroidImei;
	}
	public int getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}
	public String getIdsurvey() {
		return idsurvey;
	}
	
	public void setIdsurvey(String idsurvey) {
		this.idsurvey = idsurvey;
	}
	@Override
	public String toString() {
		return "GispdamUsersAndroid [id=" + id + ", kodeCabang=" + kodeCabang + ", loginName=" + loginName
				+ ", password=" + password + ", anroidImei=" + anroidImei + ", userStatus=" + userStatus + ", idsurvey="
				+ idsurvey + "]";
	}
	
	
}
