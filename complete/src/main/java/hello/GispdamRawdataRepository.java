package hello;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "gispdamRawdata", path = "gispdamRawdata")
public interface GispdamRawdataRepository extends PagingAndSortingRepository<GispdamRawdata, String> {
	
}
