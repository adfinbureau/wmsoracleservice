package hello;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "gispdamPhotoCollect", path = "gispdamPhotoCollect")
public interface GispdamPhotoCollectRepository extends PagingAndSortingRepository<GispdamPhotoCollect, String> {

	List<GispdamPhotoCollect> findAllByLogitudeAndLatitude(String valueOf, String valueOf2);

}
