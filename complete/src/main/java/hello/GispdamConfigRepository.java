package hello;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "gispdamConfig", path = "gispdamConfig")
public interface GispdamConfigRepository extends PagingAndSortingRepository<GispdamConfig, Integer> {

}
