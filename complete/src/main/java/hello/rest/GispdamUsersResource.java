package hello.rest;

import java.net.URISyntaxException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.GispdamUsersAndroid;
import hello.GispdamUsersAndroidRepository;

/**
 * REST controller for managing Location.
 */
@RestController
@RequestMapping("/api")
public class GispdamUsersResource {

    private final Logger log = LoggerFactory.getLogger(GispdamUsersResource.class);
        
    @Autowired
    private GispdamUsersAndroidRepository gispdamUsersAndroidRepository;

    
    /**
     * GET  /locations : get all the locations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of locations in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/user/{username}/{password}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> getUser(@PathVariable String username, @PathVariable String password)
        throws URISyntaxException {
    	System.out.println("TEST REST request to get login");
    	Optional<GispdamUsersAndroid> gispdamUser = gispdamUsersAndroidRepository.findByLoginName(username);
    	if(gispdamUser.get().getPassword().contentEquals(password)){
    		return new ResponseEntity<>(HttpStatus.OK);
    	}
    	else{
    		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    	}
    }

}
