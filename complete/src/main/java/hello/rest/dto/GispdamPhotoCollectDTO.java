package hello.rest.dto;

import java.util.Arrays;

public class GispdamPhotoCollectDTO {
	private String id;
	private String idstyle;
	private String latitude;
	private String logitude;
	private String elevasi;
	private String name;
	private String idsurvey;
	private String username;
	private String type;
	private byte[] picture;
	private byte[] video;
	private byte[] asbuild;
	private String version;
	private String serverId;
	private String description;
	private String masalah;
	private String usulan;
	private String fileName;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdstyle() {
		return idstyle;
	}
	public void setIdstyle(String idstyle) {
		this.idstyle = idstyle;
	}
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLogitude(String logitude) {
		this.logitude = logitude;
	}
	
	public String getLogitude() {
		return logitude;
	}
	
	public String getElevasi() {
		return elevasi;
	}
	public void setElevasi(String elevasi) {
		this.elevasi = elevasi;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIdsurvey() {
		return idsurvey;
	}
	public void setIdsurvey(String idsurvey) {
		this.idsurvey = idsurvey;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public byte[] getPicture() {
		return picture;
	}
	public void setPicture(byte[] picture) {
		this.picture = picture;
	}
	public byte[] getVideo() {
		return video;
	}
	public void setVideo(byte[] video) {
		this.video = video;
	}
	public byte[] getAsbuild() {
		return asbuild;
	}
	public void setAsbuild(byte[] asbuild) {
		this.asbuild = asbuild;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getServerId() {
		return serverId;
	}
	
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMasalah() {
		return masalah;
	}
	public void setMasalah(String masalah) {
		this.masalah = masalah;
	}
	public String getUsulan() {
		return usulan;
	}
	public void setUsulan(String usulan) {
		this.usulan = usulan;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	@Override
	public String toString() {
		return "GispdamPhotoCollectDTO [id=" + id + ", idstyle=" + idstyle + ", latitude=" + latitude + ", logitude="
				+ logitude + ", name=" + name + ", idsurvey=" + idsurvey + ", username=" + username + ", type=" + type
				+ ", version=" + version + ", serverId=" + serverId + ", description="
				+ description + ", masalah=" + masalah + ", usulan=" + usulan + ", fileName=" + fileName + "]";
	}


}
