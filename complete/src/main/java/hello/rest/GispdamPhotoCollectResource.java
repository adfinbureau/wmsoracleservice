package hello.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.GispdamCollectDetail1;
import hello.GispdamCollectDetail1Repository;
import hello.GispdamCollectDetail2;
import hello.GispdamCollectDetail2Repository;
import hello.GispdamCollectDetail3;
import hello.GispdamCollectDetail3Repository;
import hello.GispdamConfig;
import hello.GispdamConfigRepository;
import hello.GispdamPhotoCollect;
import hello.GispdamPhotoCollectRepository;
import hello.GispdamRawdata;
import hello.GispdamRawdataRepository;
import hello.GispdamResponse;
import hello.GispdamUsersAndroid;
import hello.GispdamUsersAndroidRepository;
import hello.rest.dto.GispdamPhotoCollectDTO;

/**
 * REST controller for managing Location.
 */
@RestController
@RequestMapping("/api")
public class GispdamPhotoCollectResource {

    private final Logger log = LoggerFactory.getLogger(GispdamPhotoCollectResource.class);
        
    @Autowired
    private GispdamPhotoCollectRepository gispdamPhotoCollectRepository;
    
    @Autowired
    private GispdamCollectDetail1Repository gispdamCollectDetail1Repository;
    
    @Autowired
    private GispdamCollectDetail2Repository gispdamCollectDetail2Repository;

    @Autowired
    private GispdamCollectDetail3Repository gispdamCollectDetail3Repository;

    @Autowired
    private GispdamRawdataRepository gispdamRawdataRepository;
    
    @Autowired
    private GispdamUsersAndroidRepository gispdamUsersAndroidRepository;
    
    @Autowired
    private GispdamConfigRepository gispdamConfigRepository;
    
    /**
     * POST  /locations : Create a new location.
     *
     * @param location the location to create
     * @return the ResponseEntity with status 201 (Created) and with body the new location, or with status 400 (Bad Request) if the location has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/gispdamPhotoCollect",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GispdamResponse> createLocation(@RequestBody GispdamPhotoCollectDTO gispdamPhotoCollectDTO) throws URISyntaxException {
        System.out.println("REST request to save GispdamPhotoCollect : "+gispdamPhotoCollectDTO);
        
        if (gispdamPhotoCollectDTO.getId() != null) {
        	GispdamResponse response = updateData(gispdamPhotoCollectDTO);
	        return ResponseEntity.ok().body(response);
        }
        else{
        	GispdamResponse response = saveData(gispdamPhotoCollectDTO);
	        return ResponseEntity.ok().body(response);
        }
//        return ResponseEntity.created(new URI("/api/gispdamPhotoCollect/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert("location", result.getId().toString()))
//            .body(null);
    }
    
    private GispdamResponse updateData(GispdamPhotoCollectDTO gispdamPhotoCollectDTO) {
		// TODO Auto-generated method stub
    	String serverId = gispdamPhotoCollectDTO.getServerId();
    	
    	if(serverId != null){
	    	System.out.println("Update Server Id : "+serverId);
	    	GispdamPhotoCollect x = gispdamPhotoCollectRepository.findOne(serverId);

	    	return Optional.ofNullable(x)
	                .map(result -> {
	                	String description = gispdamPhotoCollectDTO.getDescription();
	                	x.setName(gispdamPhotoCollectDTO.getDescription());
	                	gispdamPhotoCollectRepository.save(x);
	                	
	                	if(gispdamPhotoCollectDTO.getType().contentEquals("PICTURE")){
	                		GispdamCollectDetail1 photo = gispdamCollectDetail1Repository.findOneByFileName(gispdamPhotoCollectDTO.getFileName());
	                		photo.setDisplayName(description);
	                		photo.setDescription(description);
	                		gispdamCollectDetail1Repository.save(photo);
	                		
	                		System.out.println("SAVE UPDATE PICTURE "+photo);
	                	}
	                	else if(gispdamPhotoCollectDTO.getType().contentEquals("VIDEO")){
	                		GispdamCollectDetail2 video = gispdamCollectDetail2Repository.findOneByFileName(gispdamPhotoCollectDTO.getFileName());
	                		video.setDisplayName(description);
	                		video.setDescription(description);
	                		gispdamCollectDetail2Repository.save(video);
	                		
	                		System.out.println("SAVE UPDATE VIDEO "+video);
	                	}
	                	else if(gispdamPhotoCollectDTO.getType().contentEquals("ASBUILD")){
	                		GispdamCollectDetail3 asbuild = gispdamCollectDetail3Repository.findOneByFileName(gispdamPhotoCollectDTO.getFileName());
	                		asbuild.setDisplayName(description);
	                		asbuild.setDescription(description);
	                		gispdamCollectDetail3Repository.save(asbuild);
	                		
	                		System.out.println("SAVE UPDATE ASBUILD "+asbuild);
	                	}
	                	else if(gispdamPhotoCollectDTO.getType().contentEquals("MASALAH")){
	                		
	                	}
	                	GispdamResponse resp = new GispdamResponse();
	                	resp.setId(gispdamPhotoCollectDTO.getServerId());
	                	return resp;
	                })
	                .orElse(new GispdamResponse());
    	}
    	
    	return new GispdamResponse();
	}

	private GispdamResponse saveData(GispdamPhotoCollectDTO gispdamPhotoCollectDTO){
		System.out.println("REST REQUEST DATA : "+gispdamPhotoCollectDTO);
		
    	Optional<GispdamUsersAndroid> userOptional =  gispdamUsersAndroidRepository.findByLoginName(gispdamPhotoCollectDTO.getUsername());
    	GispdamUsersAndroid user = null;
        if(userOptional.isPresent()){
        	System.out.println("USER : "+user);
        	user = userOptional.get();
        }
        else{
        	GispdamResponse response = new GispdamResponse();
        	response.setId("NULL");
        	response.setFileName("NULL");
        	return response;
        }
                
		GispdamConfig configuration = gispdamConfigRepository.findOne(2);		
		GispdamResponse response = new GispdamResponse();
        
		if(gispdamPhotoCollectDTO.getType().contentEquals("PICTURE")){
			GispdamPhotoCollect result = saveGispdamPhotoCollect(gispdamPhotoCollectDTO, user);
			response.setId(result.getId());
			System.out.println("SAVE PICTURE");
        	byte[] pict = gispdamPhotoCollectDTO.getPicture();
        	
        	String filename = UUID.randomUUID().toString();
			try {
				System.out.println("TRY CREATE FOLDER");
				String location = configuration.getValue()+user.getKodeCabang()+"/Photo";
				File files = new File(location);
				if(!files.exists()){
					System.out.println("CREATE FOLDER");
					files.mkdirs();
					System.out.println("AFTER CREATE FOLDER");
				}else{
					System.out.println("FOLDER ALREADY EXISTS");
				}
				
				System.out.println("CREATE FILE "+location+"/"+filename+".jpg");
				FileOutputStream fos = new FileOutputStream(location+"/"+filename+".jpg");
	        	fos.write(pict);
	        	fos.close();
				System.out.println("AFTER CREATE FILE "+location+"/"+filename+".jpg");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("ERROR CREATE FILE "+e.getMessage());
			}finally{
				System.out.println("PREPARE SAVE PHOTO");
		        GispdamCollectDetail1 photo = new GispdamCollectDetail1();
		        photo.setId(filename);
		        photo.setIdheader(result.getId());
		        photo.setKodeCabang(user.getKodeCabang());
		        photo.setDescription(gispdamPhotoCollectDTO.getName());
		        photo.setFileName(filename+".jpg");
		        
		        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			     // Get the date today using Calendar object.
			     Date today = Calendar.getInstance().getTime();        
			     // Using DateFormat format method we can create a string 
			     // representation of a date with the defined format.
			     String reportDate = df.format(today);
			     
//		        photo.setDateCreated(reportDate);
//		        photo.setDateUploaded(new Date().toString());
//		        photo.setDateFile(new Date().toString());
		        gispdamCollectDetail1Repository.save(photo);
				response.setFileName(filename+".jpg");
		        System.out.println("GISPDAMCOLLECTDETAIL1 : "+photo);
				System.out.println("END SAVE PHOTO");
			}
        }
        else if(gispdamPhotoCollectDTO.getType().contentEquals("VIDEO")){
        	GispdamPhotoCollect result = saveGispdamPhotoCollect(gispdamPhotoCollectDTO, user);
			System.out.println("SAVE VIDEO");
			response.setId(result.getId());

        	byte[] vids = gispdamPhotoCollectDTO.getVideo();
        	
        	String filename = UUID.randomUUID().toString();
			try {
				System.out.println("TRY CREATE FOLDER");
				String location = configuration.getValue()+user.getKodeCabang()+"/Video";
				File files = new File(location);
				if(!files.exists()){
					System.out.println("CREATE FOLDER");
					files.mkdirs();
					System.out.println("AFTER CREATE FOLDER");
				}else{
					System.out.println("FOLDER ALREADY EXISTS");
				}
				
				System.out.println("CREATE FILE VIDEO "+location+"/"+filename+".mp4");
				FileOutputStream fos = new FileOutputStream(location+"/"+filename+".mp4");
	        	fos.write(vids);
	        	fos.close();
				System.out.println("AFTER CREATE FILE VIDEO "+location+"/"+filename+".mp4");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("ERROR CREATE FILE VIDEO "+e.getMessage());
			}finally{
		        GispdamCollectDetail2 video = new GispdamCollectDetail2();
		        video.setId(filename);
		        video.setIdheader(result.getId());
		        video.setDescription(gispdamPhotoCollectDTO.getName());
		        video.setKodeCabang(user.getKodeCabang());
//		        video.setDateCreated(new Date().toString());
//		        video.setDateUploaded(new Date().toString());
//		        video.setDateFile(new Date().toString());
		        video.setFileName(filename+".mp4");
		        gispdamCollectDetail2Repository.save(video);
				response.setFileName(filename+".jpg");
		        System.out.println("GISPDAMCOLLECTDETAIL2 : "+video);
				System.out.println("END SAVE VIDEO");
			}
        }
        else if(gispdamPhotoCollectDTO.getType().contentEquals("ASBUILD")){
        	GispdamPhotoCollect result = saveGispdamPhotoCollect(gispdamPhotoCollectDTO, user);
			response.setId(result.getId());
        	System.out.println("SAVE AS BUILD");
        	byte[] asbuild = gispdamPhotoCollectDTO.getAsbuild();
        	
        	String filename = UUID.randomUUID().toString();
			try {
				System.out.println("TRY CREATE FOLDER");
				String location = configuration.getValue()+user.getKodeCabang()+"/Asbuild";
				File files = new File(location);
				if(!files.exists()){
					System.out.println("CREATE FOLDER");
					files.mkdirs();
					System.out.println("AFTER CREATE FOLDER");
				}else{
					System.out.println("FOLDER ALREADY EXISTS");
				}
				
				System.out.println("CREATE FILE "+location+"/"+filename+".jpg");
				FileOutputStream fos = new FileOutputStream(location+"/"+filename+".jpg");
	        	fos.write(asbuild);
	        	fos.close();
				System.out.println("AFTER CREATE FILE "+location+"/"+filename+".jpg");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("ERROR CREATE FILE "+e.getMessage());
			}finally{
		        GispdamCollectDetail3 asb = new GispdamCollectDetail3();
		        asb.setId(filename);
		        asb.setIdheader(result.getId());
		        asb.setDescription(gispdamPhotoCollectDTO.getName());
		        asb.setKodeCabang(user.getKodeCabang());
//		        asb.setDateCreated(new Date().toString());
//		        asb.setDateUploaded(new Date().toString());
//		        asb.setDateFile(new Date().toString());
		        asb.setFileName(filename+".jpg");
		        gispdamCollectDetail3Repository.save(asb);
		        
				response.setFileName(filename+".jpg");
		        System.out.println("GISPDAMCOLLECTDETAIL3 : "+asb);
				System.out.println("END SAVE ASBUILD");
			}
        }
        else if(gispdamPhotoCollectDTO.getType().contentEquals("NODE")){
        	response.setId("");
        	saveGispdamRawdata(gispdamPhotoCollectDTO, user);
        }
        
		return response;
    }
    
    private GispdamRawdata saveGispdamRawdata(GispdamPhotoCollectDTO gispdamPhotoCollectDTO, GispdamUsersAndroid user){
    	GispdamRawdata node = new GispdamRawdata();
    	node.setCodepdam(user.getKodeCabang());
    	node.setLatitude(gispdamPhotoCollectDTO.getLatitude());
    	node.setLongitude(gispdamPhotoCollectDTO.getLogitude());
    	node.setElevasi(gispdamPhotoCollectDTO.getElevasi());
    	gispdamRawdataRepository.save(node);
    	return node;
    }
    
    private GispdamPhotoCollect saveGispdamPhotoCollect(GispdamPhotoCollectDTO gispdamPhotoCollectDTO, GispdamUsersAndroid user) {
		// TODO Auto-generated method stub
    	GispdamPhotoCollect x = new GispdamPhotoCollect();
        if(!gispdamPhotoCollectRepository.findAllByLogitudeAndLatitude(gispdamPhotoCollectDTO.getLogitude(), gispdamPhotoCollectDTO.getLatitude()).isEmpty()){
        	x = gispdamPhotoCollectRepository.findAllByLogitudeAndLatitude(gispdamPhotoCollectDTO.getLogitude(), gispdamPhotoCollectDTO.getLatitude()).get(0);
        }
        else{        
	        x.setId(UUID.randomUUID().toString());
	        x.setIdstyle(gispdamPhotoCollectDTO.getIdstyle());
	        x.setIdsurvey(gispdamPhotoCollectDTO.getIdsurvey());
	        x.setLatitude(gispdamPhotoCollectDTO.getLatitude());
	        x.setLogitude(gispdamPhotoCollectDTO.getLogitude());
	        x.setName(gispdamPhotoCollectDTO.getName());
	        x.setIdAndroid(user.getId()+"");
	        x.setKodeCabang(user.getKodeCabang());
        }
        GispdamPhotoCollect result = gispdamPhotoCollectRepository.save(x);
		System.out.println("GISPDAMPHOTOCOLLECT : "+result);
		return result;
	}

	/**
     * GET  /locations : get all the locations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of locations in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/gispdamPhotoCollect",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<GispdamPhotoCollect>> getAllLocations(Pageable pageable)
        throws URISyntaxException {
//        log.debug("REST request to get a page of GispdamPhotoCollect");
    	System.out.println("TEST REST request to get a page of GispdamPhotoCollect");
        Page<GispdamPhotoCollect> page = gispdamPhotoCollectRepository.findAll(pageable); 
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/locations");
        return new ResponseEntity<>(page.getContent(), null, HttpStatus.OK);
    }

}
