package hello;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "dept", path = "dept")
public interface DeptRepository extends PagingAndSortingRepository<Dept, Long> {

}
