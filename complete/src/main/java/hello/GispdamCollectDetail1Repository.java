package hello;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "gispdamCollectDetail1", path = "gispdamCollectDetail1")
public interface GispdamCollectDetail1Repository extends PagingAndSortingRepository<GispdamCollectDetail1, String> {
	GispdamCollectDetail1 findOneByIdheader(String id);

	GispdamCollectDetail1 findOneByFileName(String fileName);
}
